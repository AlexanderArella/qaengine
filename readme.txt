Link to website: https://qaengineproject20200521102933.azurewebsites.net/ (no longer functional due to the free student Azure subscription expiring)
Known issue: There are two unused views; the UpvoteQuestion view and the UpvoteAnswer view. They were created
because I used an href that linked to their respective actions. I'm not sure if this is an issue, but it's
worth mentioning.
Another issue worth mentioning is that one user can make multiple of the same upvotes.

New issue: No more access to Azure SQL, only the source code is available.
