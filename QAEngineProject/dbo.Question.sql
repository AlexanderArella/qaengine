﻿CREATE TABLE [dbo].[Question] (
    [QuestionId] INT           NOT NULL IDENTITY,
    [Title]      NVARCHAR (50) NULL,
    [Content]    NVARCHAR (50) NULL,
    [UserName]   NVARCHAR (30) NOT NULL,
    [Date]       DATE          NOT NULL,
    [Views]      INT           DEFAULT ((0)) NULL,
    [Upvotes]    INT           DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([QuestionId] ASC),
    CONSTRAINT [FK_Question_User] FOREIGN KEY ([UserName]) REFERENCES [dbo].[User] ([UserName])
);

