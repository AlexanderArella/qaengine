﻿CREATE TABLE [dbo].[AnswerComment] (
    [ACommentId] INT             NOT NULL IDENTITY,
    [AnswerId]   INT             NOT NULL,
    [Content]    NVARCHAR (1000) NULL,
    [UserName]   NVARCHAR (30)   NOT NULL,
    [Date]       DATE            NOT NULL,
    PRIMARY KEY CLUSTERED ([ACommentId] ASC),
    CONSTRAINT [FK_AnswerComment_Answer] FOREIGN KEY ([AnswerId]) REFERENCES [dbo].[Answer] ([AnswerId]),
    CONSTRAINT [FK_AnswerComment_User] FOREIGN KEY ([UserName]) REFERENCES [dbo].[User] ([UserName])
);

