﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QAEngineProject.Models;
using System.Web.Mvc;
using System.Web.Security;

namespace QAEngineProject.Controllers
{
    public class AccountController : Controller
    {
        // GET: /Account/Login
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string Password, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    var query = (from a in db.Users
                                 where a.UserName.Equals(username) && a.Password.Equals(Password)
                                 select a).SingleOrDefault();
                    if (query == null)
                    {
                        ModelState.AddModelError("", "Invalid username or password");
                        ViewBag.ReturnUrl = ReturnUrl;
                        return View();
                    }
                    FormsAuthentication.RedirectFromLoginPage(username, false);
                }
            }
            return View();
        }



        // GET: /Account/Register
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserName,Password,LastName,FirstName,Email,Country")] User user)
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
            }
            return View();
        }
    }
}