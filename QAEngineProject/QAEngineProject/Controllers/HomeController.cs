﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using QAEngineProject.Models;
using System.Web.Mvc;

namespace QAEngineProject.Controllers
{
    public class HomeController : Controller
    {
        // GET: /Home/Index
        public ActionResult Index()
        {
            List<Question> qList = null;
            using (var db = new proj1738882Entities())
            {
                var query = from a in db.Questions
                            orderby a.Date
                            select a;
                qList = query.ToList();
            }
            return View(qList);
        }


        // GET: /Home/Details/id
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = null;
            using (var db = new proj1738882Entities())
            {
                question = db.Questions.Find(id);
                question.Views++;
                var comments = (from a in db.QuestionComments
                                where a.QuestionId == question.QuestionId
                                orderby a.Date
                                select a).ToList();
                ViewBag.QComments = comments;
                var answers = (from a in db.Answers
                               where question.QuestionId == a.QuestionId
                               orderby a.Date
                               select a).ToList();
                ViewBag.Answers = answers;
                db.SaveChanges();
            }
            if (question == null)
            {
                return HttpNotFound();
            }

            return View(question);
        }

        [Authorize]
        public ActionResult UpvoteQuestion(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    Question question = (from a in db.Questions
                                         where a.QuestionId == id
                                         select a).SingleOrDefault();
                    if (question == null)
                    {
                        return HttpNotFound();
                    }
                    question.Upvotes++;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Details/" + id);
        }


        [Authorize]
        public ActionResult UpvoteAnswer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int id2 = 0;
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    Answer answer = (from a in db.Answers
                                         where a.AnswerId == id
                                         select a).SingleOrDefault();
                    if (answer == null)
                    {
                        return HttpNotFound();
                    }
                    answer.Upvotes++;
                    id2 = answer.QuestionId;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Details/" + id2);
        }

        public ActionResult ViewAnswerComments(int? id)
        {
            Answer currAnswer;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<AnswerComment> list = null;
            using (var db = new proj1738882Entities())
            {
                list = (from a in db.AnswerComments
                        where a.AnswerId == id
                        select a).ToList();
                currAnswer = (from a in db.Answers
                              where a.AnswerId == id
                              orderby a.Date
                              select a).SingleOrDefault();
                if (list == null || currAnswer == null)
                {
                    return HttpNotFound();
                }
            }
            ViewBag.CurrentAnswer = currAnswer;
            return View(list);
        }

        public ActionResult ViewQuestionComments(int? id)
        {
            Question currQuestion;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<QuestionComment> list = null;
            using (var db = new proj1738882Entities())
            {
                list = (from a in db.QuestionComments
                        where a.QuestionId == id
                        select a).ToList();
                currQuestion = (from a in db.Questions
                                where a.QuestionId == id
                                orderby a.Date
                                select a).SingleOrDefault();
                if (list == null)
                {
                    return HttpNotFound();
                }
            }
            ViewBag.CurrentQuestion = currQuestion;
            return View(list);
        }

        // GET: /Home/NewQuestion
        [Authorize]
        public ActionResult NewQuestion()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewQuestion([Bind(Include = "Title,Content,UserName,Date,Views,Upvotes")] Question q)
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    db.Questions.Add(q);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        // GET: /Home/NewAnswer
        [Authorize]
        public ActionResult NewAnswer(int? id)
        {
            Question question = null;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new proj1738882Entities())
            {
                question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
            }
            ViewBag.QuestionId = question.QuestionId;
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewAnswer([Bind(Include ="QuestionId,Content,UserName,Date,Upvotes")] Answer answer) //upvotes, date, username and QID as hidden form fields
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    db.Answers.Add(answer);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        // GET: /Home/NewAnswerComment
        [Authorize]
        public ActionResult NewAnswerComment(int? id)
        {
            Answer answer = null;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new proj1738882Entities())
            {
                answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
            }
            ViewBag.AnswerId = answer.AnswerId;
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewAnswerComment([Bind(Include = "AnswerId,Content,UserName,Date")] AnswerComment answerComment) //AnswerId, Content, Date and hidden fields
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    db.AnswerComments.Add(answerComment);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        // GET: /Home/NewQuestionComment
        [Authorize]
        public ActionResult NewQuestionComment(int? id)
        {
            Question question = null;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new proj1738882Entities())
            {
                question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
            }
            ViewBag.QuestionId = question.QuestionId;
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewQuestionComment([Bind(Include = "QuestionId,Content,UserName,Date")] QuestionComment qComment) //QuestionId, UserName, Date as hidden fields
        {
            if (ModelState.IsValid)
            {
                using (var db = new proj1738882Entities())
                {
                    db.QuestionComments.Add(qComment);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
    }
}