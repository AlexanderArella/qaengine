//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QAEngineProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class AnswerComment
    {
        public int ACommentId { get; set; }
        public int AnswerId { get; set; }
        [StringLength(1000, ErrorMessage = "Must be below 1000 characters")]
        [Display(Name = "Comment:")]
        public string Content { get; set; }
        public string UserName { get; set; }
        public System.DateTime Date { get; set; }
    
        public virtual Answer Answer { get; set; }
        public virtual User User { get; set; }
    }
}
