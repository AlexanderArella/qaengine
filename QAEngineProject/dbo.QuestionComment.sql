﻿CREATE TABLE [dbo].[QuestionComment] (
    [QCommentId] INT             NOT NULL IDENTITY,
    [QuestionId] INT             NOT NULL,
    [Content]    NVARCHAR (1000) NULL,
    [UserName]   NVARCHAR (30)   NOT NULL,
    [Date]       DATE            NOT NULL,
    PRIMARY KEY CLUSTERED ([QCommentId] ASC),
    CONSTRAINT [FK_QuestionComment_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([QuestionId]),
    CONSTRAINT [FK_QuestionComment_User] FOREIGN KEY ([UserName]) REFERENCES [dbo].[User] ([UserName])
);

