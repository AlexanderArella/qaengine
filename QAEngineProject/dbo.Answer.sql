﻿CREATE TABLE [dbo].[Answer] (
    [AnswerId]   INT             NOT NULL IDENTITY,
    [QuestionId] INT             NOT NULL,
    [Content]    NVARCHAR (1000) NULL,
    [UserName]   NVARCHAR (30)   NOT NULL,
    [Date]       DATE            NOT NULL,
    [Upvotes]    INT             DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([AnswerId] ASC),
    CONSTRAINT [FK_Question_Answer] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([QuestionId]),
    CONSTRAINT [FK_Answer_User] FOREIGN KEY ([UserName]) REFERENCES [dbo].[User] ([UserName])
);

