﻿CREATE TABLE [dbo].[User] (
    [UserName]  NVARCHAR (30) NOT NULL,
    [Password]  NVARCHAR (30) NOT NULL,
    [LastName]  NVARCHAR (30) NOT NULL,
    [FirstName] NVARCHAR (30) NOT NULL,
    [Email]     NVARCHAR (75) NOT NULL,
    [Country]   NVARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([UserName] ASC)
);

